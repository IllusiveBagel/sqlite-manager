module.exports = {
  Create: function (element, data){
    element.innerHTML = "";
    let headers = Object.keys(data[0]);

    let headerRow = element.insertRow(0);
    headers.forEach((item, index) => {
      let header = document.createElement("th");
      header.innerHTML = item;
      headerRow.appendChild(header);
    });

    data.forEach((item, index) => {
      let row = element.insertRow(index + 1);
      headers.forEach((column, colIndex) => {
        let cell = row.insertCell(colIndex);
        cell.innerHTML = item[column];
      });
    });
  }
}
