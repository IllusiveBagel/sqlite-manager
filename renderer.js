// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const grid = require('./grid');

let db;
let gridElement = document.querySelector('#grid');

function openDatabase(path) {
  console.log(path);
  db = new sqlite3.Database(path, sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
      console.error(err.message);
    }
    else{
      console.log('Connected to the SQLite database.');
      populateTables();
    }
  });
}

function populateTables(){
  let tablesElement = document.querySelector('#tables');
  let sqlInput = document.querySelector('#sql');
  let openDB = document.querySelector('#openDB');
  tablesElement.innerHTML = "";
  openDB.style.display = "none";
  sqlInput.style.display = "block";
  db.serialize(() => {
    db.all("SELECT name FROM sqlite_master WHERE type='table'", (err, tables) => {
      if (err) {
        console.log(err.message);
      }
      else{
        tables.forEach( (item, index) => {
          tablesElement.innerHTML += "<li onclick='getAll(\"" + item.name + "\")'>" + item.name + "</li>";
        });
        let container = document.querySelector('.grid-container');
        container.style.display = "block";
      }
    });
  });
}

function getAll(table){
  db.serialize(() => {
    db.all("SELECT * FROM " + table, (err, data) => {
      if (err){
        console.log(err);
      }
      else{
        grid.Create(gridElement, data);
      }
    });
  });
}

function runSQL(){
  let input = document.querySelector('#sqlInput');
  let arr = input.value.split(';');

  arr.forEach((command, index) => {
    let words = command.split(" ");
    if(words[0].toLowerCase().includes("select")){
      db.serialize(() => {
        db.all(arr[index].replace(/(\r\n|\n|\r)/gm, ""), (err, data) => {
          if (err){
            console.log(err);
          }
          else{
            grid.Create(gridElement, data);
          }
        });
      });
    }
    else{
      db.serialize(() => {
        db.run(arr[index].replace(/(\r\n|\n|\r)/gm, ""), (err, data) => {
          if (err){
            console.log(err);
          }
          else{
            grid.Create(gridElement, data);
          }
        });
      });
    }
  });
}

document.querySelector('#path').addEventListener('change', () => {
    let path = document.querySelector('#path').files[0].path;
    openDatabase(path);
})
